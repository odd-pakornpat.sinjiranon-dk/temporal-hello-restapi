package com.example.helloworld.controller

import com.example.helloworld.service.HelloService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
class HtmlController(val service: HelloService) {

    @GetMapping
    fun hello(@RequestParam(name="name") name: String): String {
        service.hello(name)
        return "OK"
    }
}