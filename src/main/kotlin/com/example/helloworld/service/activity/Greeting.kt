package com.example.helloworld.service.activity

import io.temporal.activity.ActivityInterface
import io.temporal.activity.ActivityMethod

@ActivityInterface
interface Greeting {

    @ActivityMethod
    fun composeGreeting(name: String): String
}