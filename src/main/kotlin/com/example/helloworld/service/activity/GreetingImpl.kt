package com.example.helloworld.service.activity

class GreetingImpl: Greeting {
    override fun composeGreeting(name: String): String {
        return "Hello, $name"
    }
}