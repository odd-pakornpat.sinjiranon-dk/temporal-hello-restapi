package com.example.helloworld.service

import com.example.helloworld.service.workflow.HelloWorkFlow
import io.temporal.client.WorkflowClient
import io.temporal.client.WorkflowOptions
import io.temporal.serviceclient.WorkflowServiceStubs
import org.springframework.stereotype.Service

@Service
class HelloService {

    fun hello(name: String) {
        val service = WorkflowServiceStubs.newInstance()
        val client = WorkflowClient.newInstance(service)
        val options = WorkflowOptions.newBuilder()
            .setTaskQueue("HELLO_TASK_QUEUE")
            .build()

        val workflow = client.newWorkflowStub(HelloWorkFlow::class.java, options)
        WorkflowClient.start(workflow::getGreeting, name)
    }
}