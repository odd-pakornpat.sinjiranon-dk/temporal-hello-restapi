package com.example.helloworld.service.workflow

import io.temporal.workflow.WorkflowInterface
import io.temporal.workflow.WorkflowMethod

@WorkflowInterface
interface HelloWorkFlow {

    @WorkflowMethod
    fun getGreeting(name: String): String
}