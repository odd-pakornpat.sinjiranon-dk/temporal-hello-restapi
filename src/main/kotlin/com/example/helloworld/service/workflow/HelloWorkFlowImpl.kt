package com.example.helloworld.service.workflow

import com.example.helloworld.service.activity.Greeting
import io.temporal.activity.ActivityOptions
import io.temporal.workflow.Workflow
import java.time.Duration

class HelloWorkFlowImpl: HelloWorkFlow {

    private var activityOptions = ActivityOptions
        .newBuilder()
        .setScheduleToCloseTimeout(Duration.ofSeconds(2))
        .build()

    private var format = Workflow.newActivityStub(Greeting::class.java, activityOptions)

    override fun getGreeting(name: String): String {
        return format.composeGreeting(name)
    }
}
