package com.example.helloworldworker

import com.example.helloworld.service.activity.GreetingImpl
import com.example.helloworld.service.workflow.HelloWorkFlowImpl
import io.temporal.client.WorkflowClient
import io.temporal.serviceclient.WorkflowServiceStubs
import io.temporal.worker.Worker
import io.temporal.worker.WorkerFactory

object HelloWorker {

    @JvmStatic
    fun main(args: Array<String>) {
        val service = WorkflowServiceStubs.newInstance()
        val client = WorkflowClient.newInstance(service)
        val factory = WorkerFactory.newInstance(client)
        val worker: Worker = factory.newWorker("HELLO_TASK_QUEUE")
        worker.registerWorkflowImplementationTypes(HelloWorkFlowImpl::class.java)
        worker.registerActivitiesImplementations(GreetingImpl())
        factory.start()
    }
}


